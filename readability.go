package readability

import (
	"bufio"
	"regexp"
	"strings"
)

func GradeLevel(text string) float64 {
	score := score(countSentences(text), countWords(text), countSyllables(text))

	switch {
	case score > 100.00:
		return 4
	case score > 90.00:
		return 5
	case score > 80.00:
		return 6
	case score > 70.00:
		return 7
	case score > 60.00:
		return 9
	case score > 50.00:
		return 12
	case score > 30.00:
		return 16
	case score > 0:
		return 18
	}

	return 20
}

var (
	punctuation      = []string{".", "!", ":", ";"}
	syllablePatterns = map[string]int{
		// Count hard vowels
		`(?i)([aeiou])`:                 1,
		`(?i)([bcdfghjklmnpqrstvwxz]y)`: 1,

		// Diphthongs
		`(?i)(ay)`: -1,
		`(?i)(ai)`: -1,
		`(?i)(au)`: -1,
		`(?i)(ea)`: -1,
		`(?i)(ee)`: -1,
		`(?i)(ie)`: -1,
		`(?i)(oy)`: -1,
		`(?i)(oo)`: -1,
		`(?i)(oi)`: -1,
		`(?i)(ow)`: -1,
		`(?i)(ou)`: -1,

		// Triphthongs
		`(?i)(eau)`: -1,
		`(?i)(iou)`: -1,

		// Silent letters
		`(?i)(e$)`: -1,
		`(?i)([bcdfghjklmnpqrstvwxz]oa[bcdfghjklmnpqrstvwxz]?)`: -1,
		`(?i)([bcdfghjklmnpqrstvwxz]ao[bcdfghjklmnpqrstvwxz])`:  -1,
		`(?i)([bcdfghjklmnpqrstvwxz]ei[bcdfghjklmnpqrstvwxz])`:  -1,
		`(?i)([bcdfghjklmnpqrstvwxz]?eu[bcdfghjklmnpqrstvwxz])`: -1,
		`(?i)([bcdfghjklmnpqrstvwxz]oe[bcdfghjklmnpqrstvwxz]?)`: -1,
		`(?i)([bcdfghjklmnpqrstvwxz]ue[bcdfghjklmnpqrstvwxz]?)`: -1,
		`(?i)([bcdfghjklmnpqrstvwxz]io[bcdfghjklmnpqrstvwxz])`:  -1,
		`(?i)([bcdfghjklmnpqrstvwxz]ui[bcdfghjklmnpqrstvwxz])`:  -1,
		`(?i)([bcdfghjklmnpqrstvwxz]eo[bcdfghjklmnpqrstvwxz])`:  -1,

		// If the word ends in "le" or "les" and the letter
		// before it is a constanant.
		`(?i)([bcdfghjklmnpqrstvwxz]le)$`: 1,
	}
)

func countSentences(text string) int {
	count := 1
	for _, c := range punctuation {
		count += strings.Count(strings.TrimRight(text, c), c)
	}
	return count
}

func words(text string) []string {
	words := []string{}
	lines := bufio.NewScanner(strings.NewReader(text))
	for lines.Scan() {
		for _, w := range strings.Fields(lines.Text()) {
			words = append(words, w)
		}
	}
	return words
}

func countWords(text string) int {
	return len(words(text))
}

func countSyllables(text string) int {
	trimmed := strings.TrimSpace(text)
	for _, c := range punctuation {
		trimmed = strings.Trim(trimmed, c)
	}

	count := 0
	for _, w := range words(text) {
		for p, inc := range syllablePatterns {
			rx := regexp.MustCompile(p)
			count += (len(rx.FindAllString(w, -1)) * inc)
		}
	}

	if count < 1 {
		count = 1
	}

	return count
}

func score(sentences, words, syllables int) float64 {
	asl := float64(words) / float64(sentences)
	asw := float64(syllables) / float64(words)
	return 206.835 - (1.015 * asl) - (84.6 * asw)
}
