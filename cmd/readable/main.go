package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/bmallred/readability"
)

func main() {
	var buffers [][]byte

	// Figure out if the input is coming from an argument or a pipe.
	if isPiped() {
		// Read the contents from standard input
		buffer, err := ioutil.ReadAll(os.Stdin)
		if err != nil {
			log.Println("error reading from standard input:", err)
			return
		}
		buffers = append(buffers, buffer)
	} else {
		for _, file := range os.Args[1:] {
			buffer, err := ioutil.ReadFile(file)
			if err != nil {
				log.Println("error reading from file:", err)
				return
			}
			buffers = append(buffers, buffer)
		}
	}

	for i, buffer := range buffers {
		fmt.Printf("%d. Grade level: %f\n", i+1, readability.GradeLevel(string(buffer)))
	}
}

func isPiped() bool {
	stat, _ := os.Stdin.Stat()
	mode := stat.Mode()
	return mode&os.ModeNamedPipe != 0
}
