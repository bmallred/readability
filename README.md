# readability

Applies the [Flesch-Kincaid readability tests](https://en.wikipedia.org/wiki/Flesch%E2%80%93Kincaid_readability_tests) to bodies of text returning the
determined grade level of the language used.

## Installation

``` shell
go get -u gitlab.com/bmallred/readability/cmd/readable
```

## Usage

``` shell
./readable [file...]
```

or

``` shell
cat [file] | ./readable
```
